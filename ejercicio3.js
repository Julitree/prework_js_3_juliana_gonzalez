let letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N',
    'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'
];

let numDNI = prompt("Introduzca su número de DNI sin letra");


if (numDNI < 0 || numDNI > 99999999) {
    alert("número proporcionado no es válido");
} else {
    let letraDNI = prompt("Escribe la letra de tu DNI");
    let calculoLetra = numDNI % 23;
    //Es el array de letras y el lenght del array
    let letraEscrita = letras[calculoLetra];
    if (letraEscrita != letraDNI) {
        alert("Número o letra NO son correctos");
    } else {
        alert("Número y letra correctos!");
    }
}